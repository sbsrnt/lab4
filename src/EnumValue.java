
public class EnumValue {
	public int id;
	public int EnumId;
	public String Code;
	public String Value;
	public String EnumerationName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEnumId() {
		return EnumId;
	}
	public void setEnumId(int enumId) {
		EnumId = enumId;
	}
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	public String getValue() {
		return Value;
	}
	public void setValue(String value) {
		Value = value;
	}
	public String getEnumerationName() {
		return EnumerationName;
	}
	public void setEnumerationName(String enumerationName) {
		EnumerationName = enumerationName;
	}
}
