import java.util.HashMap;
import java.util.Map;

public class Cache {

		private static Cache instance;
	
		private static Object cacheTest = new Object();
		static Map<String, EnumValue> items = new HashMap<String, EnumValue>();
		
		public static void put(String key, EnumValue value){
			items.put(key, value);
		}
		
		public EnumValue get(String key){
			return items.get(key);
		}
		
		public void clear(String key){
			items.put(key, null);
		}
		
		public void clear(){
			items.clear();
		}
		
		public static Cache getInstance(){ //singleton
			if (instance == null) {
	            synchronized (cacheTest) {
	                if (instance == null) {
	                    instance = new Cache();
	                }
	            }
	        }
			return instance;
		}
		
		private Cache(){
		}
		
		public static String String() {
	        String str = "";
	        str += "" + getInstance();
	        
	        return str;
	    }
}
